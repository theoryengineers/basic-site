var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);

var viewRoute = '/src/front/views/';

app.set('view engine', 'jade');

app.get('/', function(req, res){
    var fullPath = __dirname + viewRoute + 'index.jade'
    //console.log(fullPath);
    //res.sendFile(fullPath);
    res.render(fullPath, {});
});

io.on('connection', function(socket){
    console.log('a connection was made');
});


http.listen(3000, function(){
   console.log('listening on *:3000');
});
